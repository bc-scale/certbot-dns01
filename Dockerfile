FROM alpine

RUN apk add --update --no-cache \
    openssl \
    ca-certificates \
    py-dnspython \
    tzdata \
    py3-pip && \
    rm -rf /var/cache/apt/*

RUN pip install certbot-dns-azure \
                certbot-dns-google \
                certbot-dns-route53 \
                certbot-dns-godaddy

# additional providers: https://pypi.org/search/?q=certbot-dns-
