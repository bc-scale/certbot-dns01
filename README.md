# https://gitlab.com/dev65asia/certbot-dns01

certbot plugin to obtain TLS certificates from Let's Encrypt for domains hosted with -- , using the DNS challenge challenge mechanism.

this image currently supports:

- certbot-dns-azure
- certbot-dns-google
- certbot-dns-route53
- certbot-dns-godaddy

additional providers: https://pypi.org/search/?q=certbot-dns-
